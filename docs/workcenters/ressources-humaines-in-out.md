



# 8 | Ressources humaines IN & OUT

## Nom du poste


Ressources humaines IN & OUT
## Avant



## Après



## Description


Gestion des engagements et des départs des employé·es
## Label


Ressources humaines
## Niveau


Opérationnel
## Compétence



## Objectif(s) pédagogique(s)



## Filière



## Activité


Créer un·e collaborateur/trice dans Odoo sur la base du dossier de postulation et imprimer la carte du personnel
Imprimer le certificat de travail et verser le salaire
## Validation


Nouvel·le employé·e dans Odoo
Carte personnel imprimée
Certificat de travail généré
Paiement du salaire
## Work instructions



## Déclencheur (événement, message)


Présence physique d’un·e collaborateur/trice en début ou en fin de “contrat”
## Flux entrant


Odoo: Dossier de postulation
## Flux sortant


Odoo: Employé·e actif / inactif
## Mechanical Turk



## Matériel


Imprimante carte personnel
Imprimante laser A4
Imprimante tickets

## Module


Standard
## Budget heures



## Budget BSM


