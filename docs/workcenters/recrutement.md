



# 91 | Recrutement

## Nom du poste


Recrutement
## Avant


N/A
## Après



## Description


La personne postule pour une fonction au sein de l’entreprise à travers le portail.
## Label


On recrute!
## Niveau


Opérationnel
## Compétence



## Objectif(s) pédagogique(s)


Découvrir les métiers et compétences du domaines ES
## Filière


BL / EE / IG
## Activité


S’inscrire pour postuler dans l’entreprise. 
A voir quelles informations collecter et quel “token” donner à la personne (confirmation de réception du dossier? ticket avec barcode?)
## Validation


Nouveau candidat dans Odoo
La personne reçoit la confirmation

## Work instructions


Bienvenue chez Aroollab! Nous recherchons des personnes exceptionnelles pour renforcer nos équipes! Votre profil nous intéresse beaucoup! 
Postulez  en appuyant sur le bouton ci-dessous.
## Déclencheur (événement, message)


Événement: postulation de la personne
## Flux entrant



## Flux sortant


Ordinateur borne ou bouton “Postuler maintenant”
## Mechanical Turk



## Matériel



## Module


Custom
## Budget heures



## Budget BSM


